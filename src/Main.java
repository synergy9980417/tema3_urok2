import java.util.Arrays;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
//        System.out.printf("Hello and welcome!");
//
//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }


//        Урок 2. Введение в методы. Return
//        Цель задания:
//        Формирование базовых навыков для работы с управляющим  оператором return
//                Задание:
//        1.
//        Напишите метод, который возвращает наиболее из двух целых чисел
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Максимум :"+findMax(7,8));
//        2.
//        Напишите метод, который возвращает наиболее из двух дробных чисел
//        Вынослив похоже, не так ли? Хотелось бы как
//                -
//                то оптимизировать?:) не спешите, это
//        все возможно: мы до этого ещё дойдём!


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        System.out.println("Максимум :"+findMax(7,8));
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Максимум :"+findDmax(7.3,8.1));
//                3.
//        Напишите метод, который возвращает самую длинную из трех строк

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("самая длинная строка: "+getMax3str("asc","aaaa","bbbbb"));
//        4.
//        Из чет
//        ырёх

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("самая длинная строка: "+getMax4str("asc","aaaa","bbbbb","asdfasdfasdfa"));
//        5.
//        Из пяти. Хочется как
//        -
//                то оптимизировать?:) это хорошо, сможем, когда изучим
//                vararg

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("самая длинная строка: "+getMax5str("asc","aaaa","bbbbb","asdfasdfasdfa","23453sdf"));

//        6.
//        Напишите метод, который возвращает входящую строку, делая ее ЗаБоРчИкОм

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println(getZabor("asdfasasdfasdf"));
//        7.
//        Напишите метод, который возвращает наибольший элемент массива

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        int [] arr7={2,3,4,5,6,7,8,9,1};
        System.out.println(getMaxInArr(arr7));

//        8.
//        Напишите метод, который принимает т
//        ри массива, а возвращает массив, сумма
//        элементов которого самая большая

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        int[] arr8_1={1,3,6,4,2,4};

        int[] arr8_2={2,3,6,4,2,4};
        int[] arr8_3={3,3,6,4,2,4};
        System.out.println("Максимальная сумма из 3-х массива, всех их элементов у этого массива:");
        System.out.println(Arrays.toString(bigArr(arr8_1,arr8_2,arr8_3)));
//        9.
//        Напишите метод, который принимает массив чисел, а возвращает исходный
//        массив, но без отрицательных чисел

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!! 9 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

int[] arr9={1,3,-3,4,3,-55,4,0};
        System.out.println(Arrays.toString(getArrPlus(arr9)));


//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана обща
//        я структура программы
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выпол
//        ненным при условии, что слушатель получил оценку не
//        менее 3 баллов



    }
    static int findMax(int a, int b){
        if (a>b) return a;
        else if (b>a) return b;
        else {
            System.out.printf("Числа равны");
            return 0;
        }
    }

    static double findDmax(double a, double b){
        if (a>b) return a;
        else if (b>a) return b;
        else {
            System.out.printf("Числа равны");
            return 0;
        }
    }
    static String getMax3str(String str1, String str2, String str3){
        if (str1.length()>=str2.length()&&str1.length()>=str3.length()) return str1;
        if (str2.length()>=str1.length()&&str2.length()>=str3.length()) return str2;
        if (str3.length()>=str1.length()&&str3.length()>=str2.length()) return str3;
        System.out.println("что-то пошло не так");
        return "";
    }

    static String getMax4str(String str1, String str2, String str3, String str4){
        if (str1.length()>=str2.length()&&str1.length()>=str3.length() &&str1.length()>=str4.length()) return str1;
        if (str2.length()>=str1.length()&&str2.length()>=str3.length()&&str2.length()>=str4.length()) return str2;
        if (str3.length()>=str1.length()&&str3.length()>=str2.length()&&str3.length()>=str4.length()) return str3;
        if (str4.length()>=str1.length()&&str4.length()>=str2.length()&&str4.length()>=str3.length()) return str4;
        System.out.println("что-то пошло не так");
        return "";
    }

    static String getMax5str(String str1, String str2, String str3, String str4, String str5){
        if (str1.length()>=str2.length()&&str1.length()>=str3.length() &&str1.length()>=str4.length()&&str1.length()>=str5.length()) return str1;
        if (str2.length()>=str1.length()&&str2.length()>=str3.length()&&str2.length()>=str4.length()&&str2.length()>=str5.length()) return str2;
        if (str3.length()>=str1.length()&&str3.length()>=str2.length()&&str3.length()>=str4.length()&&str3.length()>=str5.length()) return str3;
        if (str4.length()>=str1.length()&&str4.length()>=str2.length()&&str4.length()>=str3.length()&&str4.length()>=str5.length()) return str4;
        if (str5.length()>=str1.length()&&str5.length()>=str2.length()&&str5.length()>=str3.length()&&str5.length()>=str1.length()) return str5;
        System.out.println("что-то пошло не так");
        return "";
    }

    static String getZabor(String str){
        char[] chars = str.toCharArray();
        for (int i = 0; i<str.length(); i++)
           if (i%2==0)
               chars[i] = Character.toUpperCase(chars[i]);
           else
               chars[i] = Character.toLowerCase(chars[i]);

           return String.valueOf(chars);

    }
    static int getMaxInArr(int[] arr){
        int maxValue = Integer.MIN_VALUE;
        for (int i=0;i<arr.length;i++)
            if (maxValue<arr[i]) maxValue=arr[i];
        return maxValue;
    }

    static int[] bigArr(int[] arr1, int[] arr2, int[] arr3){
        if (sumArr(arr1)>=sumArr(arr2)&&sumArr(arr1)>=sumArr(arr3))
            return arr1;
        else
        if (sumArr(arr2)>=sumArr(arr1)&&sumArr(arr2)>=sumArr(arr3))
            return arr2;
        else return arr3;
    }
    static int sumArr(int[] arr){
        int sum=0;
        for (int i=0;i<arr.length;i++)
            sum+=arr[i];
        return sum;
    }

    static int[] getArrPlus(int[] arr){
        int j=0;
        for (int i=0;i<arr.length;i++)
            if (arr[i]>=0) {
                j++;
            }

        int[] arr1 = new int[j];
        j=0;

        for (int i=0;i<arr.length;i++)
            if (arr[i]>=0) {
                arr1[j]=arr[i];
                j++;
            }


        return arr1;

    }
}